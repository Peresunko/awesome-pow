#include <QDebug>
#include <QtCore>

float pow2(float x);
float pow3(float x);

int main(int argc, char *argv[])
{
    qDebug() << "Inter x: ";
    QTextStream s(stdin);
    QString value = s.readLine();
    float x = value.toFloat();
    float p3 = pow3(x);

    qDebug() << x << "^ 2 =" << p3;
}

float pow2(float x)
{
    return x * x;
}

float pow3(float x)
{
    return x * x * x;
}
